#! /bin/bash
#

function optimizeSvgs() {
    if ! command -v svgcleaner &> /dev/null; then
        echo -e "svgcleaner could not be found. install it from:\n" >&2
        echo -e "\thttps://github.com/RazrFalcon/svgcleaner\n"
        exit 1
    fi

    mkdir -p dist/

    echo -e "Processing SVG files..."
    sleep 1

    for file in working/*
    do
        echo -e "Optimizing ${file}"
        svgcleaner $(pwd)/$file $(pwd)/$file
        cp $(pwd)/$file dist/
        echo -e "Remove ${file}"
        rm $(pwd)/$file
    done

    echo -e "Done cleaning and copying files!\n"

    if [[ $1 && -d $1 ]]; then
        echo -e "Copying icon files to $1/icons"
        mkdir -p $1/icons
        cp dist/* $1/icons/
        echo -e "Done!\n"
    elif [[ $1 ]]; then
        echo -e "To copy files specify a valid directory\n"
    fi
}


function appPngs() {

    declare -a ICON_SIZES=(
        "16"
        "32"
        "36"
        "48"
        "57"
        "72"
        "76"
        "96"
        "120"
        "128"
        "144"
        "152"
        "167"
        "180"
        "192"
        "195"
        "196"
        "228"
        "270"
    )

    echo -e "Deleting prior .png files\n"
    cd apps/
    rm *.png
    sleep 1

    for ICON in *.svg
    do

        EXT_GET="${ICON##*.}"
        EXT="${EXT_GET,,}"
        if [[ $ICON == *"-full"* ]]; then
            ICON_NAME=$(basename "$ICON" "-full.${EXT_GET}")
        else
            ICON_NAME=$(basename "$ICON" ".${EXT_GET}")
        fi

        # Make PNG full-size
        ICON_PNG="${ICON_NAME}.png"
        echo -e "Making PNG of: $ICON -> $ICON_PNG"
        convert $ICON $ICON_PNG 

        COUNT=0
        OUT_TAGS=""

        if [[ -f $ICON_PNG ]]; then
            echo -e "Generating various sizes"

            for SIZE in "${ICON_SIZES[@]}"
            do
                SIZE_NAME="${SIZE}x${SIZE}"
                SAVE_NAME="$ICON_NAME-$SIZE_NAME.png"
                convert $ICON_PNG -resize $SIZE $SAVE_NAME
                echo "- Created ${SAVE_NAME}"
                let "COUNT++"
            done

            echo -e "✓ Created $COUNT different sized images\n"
        fi
    done
}


function publishNpm() {
    echo "Ready to publish icons to NPM?"
    read -p "Continue? [y/N]" -n 1 -r
    sleep 2
    echo -e "\nPUBLISH wheeeee"
    npm publish --access public
}


# Arguments
while (( ${#} > 0 )); do
    case "${1}" in
      ('--optimize') OPTIMIZE="true";;
      ('--apps') APPS="true";;
      ('--publish') PUBLISH="true";;
      ('--') OPANDS+=("${@:2}"); break;;
      ('-'?*) ;;
      (*) OPANDS+=("${1}")
    esac
    shift
done

if [[ $OPTIMIZE == true ]]; then
    optimizeSvgs
fi

if [[ $APPS == true ]]; then
    appPngs
fi

if [[ $PUBLISH == true ]]; then
    publishNpm
fi
