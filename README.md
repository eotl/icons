Icons
=====

A custom icon set for EOTL apps and websites used throughout EOTL's zero-waste 
sustainable logistics system.


## Contributing

While working on an icon, store it in the directory `working/` directory. 
Once you feel it is done copy it to the `final/` directory and submit a 
pull-request. 


## Building

You need to have the [svgcleaner](https://github.com/RazrFalcon/svgcleaner)
installed as a command you can run. Then run the following command:

```
$ ./build.sh
```

To copy icons to a local project / repo, pass the path arg

```
$ ./build.sh ../local/path/to/copy
```

## Publishing

Requires having NPM installed and account granted access to EOTL's organization. 
To clean, prepare and publish icons to NPM as a package, run the following.

```
$ ./build.sh --publish
```


## Credits

Some of the icons contained were purely original works. Some are derivitives of
icons found on NounProject. Please [credits](CREDITS.md) for that.
